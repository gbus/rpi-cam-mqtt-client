
import asyncio
from hbmqtt.client import MQTTClient, ClientException
from hbmqtt.mqtt.constants import QOS_1, QOS_2


# broker_uri = "mqtt://username:password@address:port"
#
# config = {
#     'keep_alive': 10,
#     'ping_delay': 1,
#     'default_qos': 1,
#     'default_retain': True,
#     'auto_reconnect': True,
#     'reconnect_max_interval': 5,
#     'reconnect_retries': 10,
# }


class MQTT(object):
    """MQTT client example."""

    # pylint: disable=unused-argument

    def __init__(self, broker_uri, config):
        """Setup MQTT client."""
        self.topics = {}
        self._mqttc = MQTTClient()
        # self._mqttc.username_pw_set(username=user, password=password)
        self._mqttc.connect(broker_uri, config)

    def publish(self, topic, payload, qos, retain):
        """Publish an MQTT message."""
        self._mqttc.publish(topic, payload, qos, retain)

    async def subscribe(self, topic, callback, qos):
        """Subscribe to an MQTT topic."""
        if topic in self.topics:
            return
        await self._mqttc.subscribe(self._mqttc, [(topic, qos)])

        try:
            while True:
                message = await self._mqttc.deliver_message()
                packet = message.publish_package

                await callback(
                    packet.variable_header.topic_name,
                    str(packet.payload.data),
                    qos
                )
            self.topics[topic] = callback
        except ClientException:
            log.exception("hbmqtt.client.ClientException")
            raise

    def start(self):
        """Run the MQTT client."""
        print('Start MQTT client')
        self.loop = asyncio.get_event_loop()
        self.loop.run_forever()

    def stop(self):
        """Stop the MQTT client."""
        print('Stop MQTT client')
        self._mqttc.disconnect()
        self.loop.close()

    # def run(self):
    #     asyncio.get_event_loop().run_until_complete(
    #         self.
    #     )

