import json
import time
import logging
import asyncio

from rpicammqtt_client.loadconfig import load_config, config_file

from rpicammqtt_client import AsyncRpiCamMqttClient
from rpicammqtt_client.hbmqtt_mqtt import MQTT

c = load_config(config_file)

numeric_level = logging.getLevelName(c['logging']['level'])
logging.basicConfig(level=numeric_level)

# logger = logging.getLogger("mqttwrapper.hbmqtt_backend")
logger = logging.getLogger()
logger.debug("Config level: %s(%i)" % (
    c['logging']['level'],
    numeric_level
))

camera = c['rpiname']
mqtt_qos = c['mqtt']['qos']
mqtt_keepalive = c['mqtt']['keepalive']

# broker_uri = "mqtt://{}:{}@{}:{}".format(
#     c['mqtt']['user'],
#     c['mqtt']['pw'],
#     c['mqtt']['server'],
#     c['mqtt']['port']
# )

broker_uri = "mqtt://{}:{}".format(
    '127.0.0.1',
    c['mqtt']['port']
)

mqtt_config = {
    'keep_alive': c['mqtt']['keepalive'],
    'ping_delay': 1,
    'default_qos': mqtt_qos,
    'default_retain': True,
    'auto_reconnect': True,
    'reconnect_max_interval': 5,
    'reconnect_retries': 10,
}

# Create an instance of the MQTT client based on paho mqtt and start it
MQTTC = MQTT(broker_uri, mqtt_config)


# Create an instance of the rpicam client
RPI_CLIENT = AsyncRpiCamMqttClient(
    MQTTC.publish,
    MQTTC.subscribe,
    camera,
    qos=mqtt_config['default_qos'],
    retain=mqtt_config['default_retain']
)


def show_status(rpic):
    print("Current status: Active({}), Recording({}), Motion({})".format(
        rpic.is_active(),
        rpic.is_recording(),
        rpic.is_detecting_motion()
    ))


MQTTC.start()

event_loop = asyncio.get_event_loop()
try:
    return_value = event_loop.run_until_complete(
        RPI_CLIENT.async_subscribe_rpicam()
    )
finally:
    event_loop.close()

# # Print out all current camera info
# print("PanTilt views: {}".format(json.dumps(RPI_CLIENT.get_ptviews(), indent=4)))

# # Watch the status and show details if it changes
# curr_status = None
# while True:
#     time.sleep(0.5)
#     show_status(RPI_CLIENT)
#     new_status = RPI_CLIENT.get_status()
#     if new_status != curr_status:
#         curr_status = new_status
#         show_status(RPI_CLIENT)

MQTTC.stop()
